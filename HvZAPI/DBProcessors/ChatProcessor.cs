﻿using Dapper;
using HvZAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.DBProcessors
{
    public class ChatProcessor
    {
        public async static Task<List<Chat>> getChatsForGame(int game_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Chat WHERE game_id = '{game_id}' AND message!='' ";
                var output = await cnn.QueryAsync<Chat>(sqlQuery, new DynamicParameters());

                return output.ToList();
            }
        }

        public async static Task<List<Chat>> getGlobalChatsForGame(int game_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Chat WHERE game_id = '{game_id}' AND is_human_global='true' AND is_zombie_global='true' ";
                var output = await cnn.QueryAsync<Chat>(sqlQuery, new DynamicParameters());

                return output.ToList();
            }
        }

        public async static Task<List<Chat>> getHumanChat(int game_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Chat WHERE game_id = '{game_id}' AND is_human_global='true' AND is_zombie_global='false'";
                var output = await cnn.QueryAsync<Chat>(sqlQuery, new DynamicParameters());

                return output.ToList();
            }
        }

        public async static Task<List<Chat>> getZombieChat(int game_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Chat WHERE game_id = '{game_id}' AND is_zombie_global='true' AND is_human_global='false'";
                var output = await cnn.QueryAsync<Chat>(sqlQuery, new DynamicParameters());

                return output.ToList();
            }
        }

        public async static Task<List<Chat>> getSquadChat(int game_id, int squad_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Chat WHERE game_id = '{game_id}' AND squad_id = '{squad_id}'";
                var output = await cnn.QueryAsync<Chat>(sqlQuery, new DynamicParameters());

                return output.ToList();
            }
        }

        public async static Task<int> sendNewMessage(int game_id, Chat newChat)
        {
            string findQuery = "";
            string insertQuery = "";

            //string findUsername = $"SELECT * FROM Player WHERE player_id='{newChat.Player_Id}' ";

            if (newChat.Player_Id == 0)
            {
                findQuery = "SELECT * FROM Chat WHERE message='' AND is_human_global='false' AND is_zombie_global='false' ";
                insertQuery = "INSERT INTO Chat (message, is_human_global, is_zombie_global, chat_time, game_id, username, is_admin) " +
                    "VALUES (@Message, @Is_Human_Global, @Is_Zombie_Global, @Chat_Time, @Game_Id, @Username, @Is_Admin) " +
                    "SELECT SCOPE_IDENTITY() as id";
                
            } else if (newChat.Squad_Id == 0)
            {
                findQuery = "SELECT * FROM Chat WHERE message='' AND is_human_global='false' AND is_zombie_global='false' ";
                insertQuery = "INSERT INTO Chat (message, is_human_global, is_zombie_global, chat_time, game_id, player_id, username, is_admin) " +
                    "VALUES (@Message, @Is_Human_Global, @Is_Zombie_Global, @Chat_Time, @Game_Id, @Player_Id, @Username, @Is_Admin) " +
                    "SELECT SCOPE_IDENTITY() as id";
            } else
            {
                findQuery = "SELECT * FROM Chat WHERE message='' AND is_human_global='false' AND is_zombie_global='false' ";
                insertQuery = "INSERT INTO Chat (message, is_human_global, is_zombie_global, chat_time, game_id, player_id, squad_id, username, is_admin) " +
                    "VALUES (@Message, @Is_Human_Global, @Is_Zombie_Global, @Chat_Time, @Game_Id, @Player_Id, @Squad_Id, @Username, @Is_Admin) " +
                    "SELECT SCOPE_IDENTITY() as id";
            }
            
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                //Player player = await cnn.QueryFirstOrDefaultAsync<Player>(findUsername);

                //if(player != null)
                //{
                    //newChat.Username = player.Username;

                Chat emptyChat = await cnn.QueryFirstOrDefaultAsync<Chat>(findQuery);
                if (emptyChat != null)
                {
                    updateMessage(game_id, emptyChat.Message_Id, newChat);

                    return emptyChat.Message_Id;
                }
                else
                {
                    int id = await cnn.ExecuteScalarAsync<int>(insertQuery, newChat);

                    return id;
                }
                //} else
                //{
                //    return -1;
                //}
            }
        }

        public async static void updateMessage(int game_id, int message_id, Chat chat)
        {
            string sqlQuery = "UPDATE Chat " +
                   $"SET message='{chat.Message}', is_human_global='{chat.Is_Human_Global}', is_zombie_global='{chat.Is_Zombie_Global}', chat_time='{chat.Chat_Time}', game_id='{chat.Game_Id}', player_id='{chat.Player_Id}', squad_id='{chat.Squad_Id}', username='{chat.Username}', is_admin='{chat.is_admin}' " +
                   $"WHERE message_id='{message_id}'";

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public async static void deleteMessage(int game_id, int message_id)
        {
            string sqlQuery = "UPDATE Chat " +
                   $"SET message='', is_human_global='false', is_zombie_global='false', username='' " +
                   $"WHERE message_id='{message_id}'";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public static string LoadConnectionString()
        {
            //return @"Data Source =.\HvZSqliteDB.db; Version = 3;";
            return @"Server = tcp:hvz.database.windows.net,1433; Initial Catalog = hvz; Persist Security Info = False; User ID = hvz; Password = Caseoppgave2019; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
        }
    }
}
