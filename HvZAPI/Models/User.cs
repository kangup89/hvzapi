﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.Models
{
    public class User
    {
        public int User_Id { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Is_Admin { get; set; }
        public string Token { get; set; }
        public byte[] Password_Hash { get; set; }
        public string Salt { get; set; }

        public User()
        {
        }
    }
}
