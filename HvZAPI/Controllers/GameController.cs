﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using HvZAPI.Models;
using HvZAPI.Models.DBProcessors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HvZAPI.Controllers
{
    //[RequireHttps]
    [Authorize]
    [Route("game")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GameController : ControllerBase
    {
        // GET game
        //[RequireHttps]
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Game>>> GetGames()
        {
            return await GameProcessor.getGames();
        }

        // GET game/:id
        //[AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<Game>> GetGameById(int id)
        {
            return await GameProcessor.getGameById(id);
        }

        // POST game
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<int> CreateNewGame([FromBody] Game newGame)
        {
            try
            {
                return await GameProcessor.createNewGame(newGame);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("{game_id}/zero")]
        public async Task<ActionResult> ChoosePatientZero(int game_id)
        {
            Debug.WriteLine("controller zero");

            try
            {
                int player_id = await GameProcessor.choosePatientZero(game_id);

                return Ok(player_id);
            } catch
            {
                return BadRequest( new { message = "Failed to choose patient zero" } );
            }
        }

        // PUT game/:id
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public bool UpdateGame(int id, [FromBody] Game game)
        {
            try
            {
                GameProcessor.updateGame(id, game);

                return true;
            } catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        // DELETE game/:id
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public bool DeleteGame(int id)
        {
            try
            {
                GameProcessor.deleteGame(id);

                return true;
            } catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
