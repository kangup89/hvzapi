﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using HvZAPI.Models;
using HvZAPI.Models.DBProcessors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace HvZAPI.Controllers
{
    [Authorize]
    [Route("game/auth")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AuthController : ControllerBase
    {
        private AuthProcessor _authProcessor;

        public AuthController(AuthProcessor authProcessor)
        {
            _authProcessor = authProcessor;
        }

        //[AllowAnonymous]
        [Authorize(Roles = "Admin")]
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<User>>> GetAllUsers()
        {
            return await AuthProcessor.getAllUsers();
        }

        // GET game/auth/authenticate
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<ActionResult> Authenticate([FromBody] User user)
        {

            User userObj = await _authProcessor.authenticate(user);

            if(userObj == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }

            return Ok(userObj.Token);
        }

        // POST game/auth
        [AllowAnonymous]
        //[Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<int> CreateNewUser([FromBody] User newUser)
        {
            try
            {
                return await AuthProcessor.createNewUser(newUser);
            } catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        //[Authorize(Roles = "Admin")]
        [Authorize]
        [HttpPut("{user_id}")]
        public bool UpdateUser(int user_id, [FromBody] User user)
        {
            try
            {
                AuthProcessor.updateUser(user_id, user);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        //[Authorize(Roles = "Admin")]
        [Authorize]
        [HttpDelete("{user_id}")]
        public bool DeleteUser(int user_id)
        {
            try
            {
                AuthProcessor.deleteUser(user_id);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}