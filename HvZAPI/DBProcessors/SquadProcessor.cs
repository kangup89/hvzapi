﻿using Dapper;
using HvZAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.DBProcessors
{
    public class SquadProcessor
    {
        public async static Task<List<Squad>> getSquadsByGameId(int game_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Squad WHERE game_id = '{game_id}' AND name!=''";
                var output = await cnn.QueryAsync<Squad>(sqlQuery);

                return output.ToList();
            }
        }

        public async static Task<Squad> getSquadBySquadId(int game_id, int squad_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Squad WHERE game_id = '{game_id}' AND squad_id='{squad_id}'";
                Squad output = await cnn.QueryFirstOrDefaultAsync<Squad>(sqlQuery);
                
                return output;
            }
        }

        public async static Task<int> createSquadForGame(int game_id, Squad newSquad)
        {
            string findQuery = "SELECT * FROM Squad WHERE name='' ";
            string insertQuery = "INSERT INTO Squad (name, is_human, game_id) " +
                "VALUES (@Name, @Is_Human, @Game_Id) " +
                "SELECT SCOPE_IDENTITY() as id";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                Squad emptySquad = await cnn.QueryFirstOrDefaultAsync<Squad>(findQuery);
                if(emptySquad != null)
                {
                    updateSquadForGame(game_id, emptySquad.Squad_Id, newSquad);

                    return emptySquad.Squad_Id;
                } else
                {
                    int id = await cnn.ExecuteScalarAsync<int>(insertQuery, newSquad);

                    return id;
                }
            }
        }

        public async static void updateSquadForGame(int game_id, int squad_id, Squad squad)
        {
            string sqlQuery = "UPDATE Squad " +
                $"SET name='{squad.Name}', is_human='{squad.Is_Human}', game_id='{squad.Game_Id}' " +
                $"WHERE squad_id='{squad_id}'";

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public async static void deleteSquad(int game_id, int squad_id)
        {
            string sqlQuery = "UPDATE Squad " +
                $"SET name='', is_human='false' " +
                $"WHERE squad_id='{squad_id}' ";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public static string LoadConnectionString()
        {
            //return @"Data Source =.\HvZSqliteDB.db; Version = 3;";
            return @"Server = tcp:hvz.database.windows.net,1433; Initial Catalog = hvz; Persist Security Info = False; User ID = hvz; Password = Caseoppgave2019; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
        }
    }
}
