﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.Models
{
    public class Kill
    {
        public int Kill_Id { get; set; }
        public DateTime Time_Of_Death { get; set; }
        public string Story { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Game_Id { get; set; }
        public int Killer_Id { get; set; }
        public int Victim_Id { get; set; }
        public string Bite_Code { get; set; }

        public Kill()
        {
        }
    }
}
