﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using HvZAPI.DBProcessors;
using HvZAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HvZAPI.Controllers
{
    [Authorize]
    [Route("game")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class KillController : ControllerBase
    {
        [HttpGet("{game_id}/kill")]
        public async Task<ActionResult<IEnumerable<Kill>>> GetKillsByGameId(int game_id)
        {
            return await KillProcessor.getKillsByGameId(game_id);
        }

        [HttpGet("{game_id}/kill/{kill_id}")]
        public async Task<ActionResult<Kill>> GetKillByKillId(int game_id, int kill_id)
        {
            return await KillProcessor.getKillByKillId(game_id, kill_id);
        }

        [HttpPost("{game_id}/kill")]
        public async Task<int> CreateKillForGame(int game_id, [FromBody] Kill newKill)
        {
            try
            {
                return await KillProcessor.createKillForGame(game_id, newKill);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{game_id}/kill/{kill_id}")]
        public bool updateKillForGame(int game_id, int kill_id, [FromBody] Kill kill)
        {
            try
            {
                KillProcessor.updateKillForGame(game_id, kill_id, kill);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{game_id}/kill/{kill_id}")]
        public bool deleteKill(int game_id, int kill_id)
        {
            try
            {
                KillProcessor.deleteKill(game_id, kill_id);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}