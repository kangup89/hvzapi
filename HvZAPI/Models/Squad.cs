﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.Models
{
    public class Squad
    {
        public int Squad_Id { get; set; }
        public string Name { get; set; }
        public bool Is_Human { get; set; }
        public int Game_Id { get; set; }

        public Squad()
        {
        }
    }
}
