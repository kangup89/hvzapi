﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using HvZAPI.DBProcessors;
using HvZAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HvZAPI.Controllers
{
    [Authorize]
    [Route("game")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SquadController : ControllerBase
    {
        [HttpGet("{game_id}/squad")]
        public async Task<ActionResult<IEnumerable<Squad>>> GetSquadsByGameId(int game_id)
        {
            return await SquadProcessor.getSquadsByGameId(game_id);
        }

        [HttpGet("{game_id}/squad/{squad_id}")]
        public async Task<ActionResult<Squad>> GetSquadBySquadId(int game_id, int squad_id)
        {
            return await SquadProcessor.getSquadBySquadId(game_id, squad_id);
        }

        [HttpPost("{game_id}/squad")]
        public async Task<int> CreateSquadForGame(int game_id, [FromBody] Squad newSquad)
        {
            try
            {
                return await SquadProcessor.createSquadForGame(game_id, newSquad);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{game_id}/squad/{squad_id}")]
        public bool UpdateSquadForGame(int game_id, int squad_id, [FromBody] Squad squad)
        {
            try
            {
                SquadProcessor.updateSquadForGame(game_id, squad_id, squad);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{game_id}/squad/{squad_id}")]
        public bool DeleteSquad(int game_id, int squad_id)
        {
            try
            {
                SquadProcessor.deleteSquad(game_id, squad_id);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}