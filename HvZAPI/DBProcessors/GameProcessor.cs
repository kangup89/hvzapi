﻿using Dapper;
using HvZAPI.DBProcessors;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.Models.DBProcessors
{
    public class GameProcessor
    {
        public async static Task<List<Game>> getGames()
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                var output = await cnn.QueryAsync<Game>("SELECT * FROM Game WHERE name!='' AND game_state!='Deleted'", new DynamicParameters());

                return output.ToList();
            }
        }

        public async static Task<Game> getGameById(int game_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Game WHERE game_id = '{game_id}'";
                Game output = await cnn.QueryFirstOrDefaultAsync<Game>(sqlQuery);

                return output;
            }
        }
        
        public async static Task<int> createNewGame(Game newGame)
        {
            string findQuery = "SELECT * FROM Game WHERE game_state='Deleted'";
            string insertQuery = "INSERT INTO Game (name, game_state, nw_lat, nw_lng, se_lat, se_lng, description, start_time, end_time, has_patient_zero) " +
                "VALUES (@Name, @Game_State, @Nw_Lat, @Nw_Lng, @Se_Lat, @Se_Lng, @Description, @Start_Time, @End_Time, @Has_Patient_Zero) " +
                "SELECT SCOPE_IDENTITY() as id";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                newGame.Has_Patient_Zero = false;

                Game emptyGame = await cnn.QueryFirstOrDefaultAsync<Game>(findQuery);
                if (emptyGame != null)
                {
                    updateGame(emptyGame.Game_Id, newGame);

                    return emptyGame.Game_Id;
                }
                else
                {
                    int id = await cnn.ExecuteScalarAsync<int>(insertQuery, newGame);

                    return id;
                }
            }
        }

        public async static void updateGame(int game_id, Game game)
        {
            string sqlQuery = "UPDATE Game " +
                $"SET name='{game.Name}', game_state='{game.Game_State}', nw_lat='{game.Nw_Lat}', nw_lng='{game.Nw_Lng}', se_lat='{game.Se_Lat}', se_lng='{game.Se_Lng}', description='{game.Description}', start_time='{game.Start_Time}', end_time='{game.End_Time}', has_patient_zero='{game.Has_Patient_Zero}' " +
                $"WHERE game_id='{game_id}'";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public async static void deleteGame(int game_id)
        {
            string sqlQuery = "UPDATE Game " +
                $"SET name='', game_state='Deleted' " +
                $"WHERE game_id='{game_id}'";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public async static Task<int> choosePatientZero(int game_id)
        {
            Game game = await getGameById(game_id);

            if(game.Has_Patient_Zero == false)
            {
                List<Player> players = await PlayerProcessor.getPlayersByGameId(game_id);

                if(players.Count != 0)
                {
                    Random r = new Random();
                    int playerIndex = r.Next(0, players.Count - 1);

                    Player player = players[playerIndex];

                    player.Is_Human = false;
                    player.Is_Patient_Zero = true;
                    player.Bite_Code = "PatientZero";

                    await PlayerProcessor.updatePlayerForGame(game_id, player.Player_Id, player);

                    SquadMember memberObj = await SquadMemberProcessor.getSquadMemberByPlayerId(game_id, player.Player_Id);

                    if (memberObj != null)
                    {
                        SquadMemberProcessor.deleteSquadMember(game_id, memberObj.Squad_Id, memberObj.Squad_Member_Id);
                    }

                    int squad_id = await SquadProcessor.createSquadForGame(game_id, new Squad { Name = "Patient_Zero", Is_Human = false, Game_Id = game_id });

                    await SquadMemberProcessor.createSquadMember(game_id, squad_id, new SquadMember { Game_Id = game_id, Player_Id = player.Player_Id, Squad_Id = squad_id });

                    game.Has_Patient_Zero = true;

                    updateGame(game_id, game);

                    return player.Player_Id;
                }

                return -1;
            }

            return -1;
        }

        public static string LoadConnectionString()
        {
            //return @"Data Source =.\HvZSqliteDB.db; Version = 3;";
            return @"Server = tcp:hvz.database.windows.net,1433; Initial Catalog = hvz; Persist Security Info = False; User ID = hvz; Password = Caseoppgave2019; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
        }
    }
}
