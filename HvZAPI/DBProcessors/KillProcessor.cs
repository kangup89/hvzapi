﻿using Dapper;
using HvZAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.DBProcessors
{
    public class KillProcessor
    {
        public async static Task<List<Kill>> getKillsByGameId(int game_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM \"Kill\" WHERE game_id = '{game_id}' AND bite_code!=''";
                var output = await cnn.QueryAsync<Kill>(sqlQuery);

                return output.ToList();
            }
        }

        public async static Task<Kill> getKillByKillId(int game_id, int kill_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM \"Kill\" WHERE game_id = '{game_id}' AND kill_id='{kill_id}'";
                Kill output = await cnn.QueryFirstOrDefaultAsync<Kill>(sqlQuery);

                return output;
            }
        }

        public async static Task<int> createKillForGame(int game_id, Kill newKill)
        {
            if (checkValidCode(game_id, newKill))
            {
                string findQuery = "SELECT * FROM \"Kill\" WHERE story='' AND bite_code='' ";
                string insertQuery = "INSERT INTO \"Kill\" (time_of_death, story, lat, lng, game_id, killer_id, victim_id, bite_code) " +
                    "VALUES (@Time_Of_Death, @Story, @Lat, @Lng, @Game_Id, @Killer_Id, @Victim_Id, @Bite_Code) " +
                    "SELECT SCOPE_IDENTITY() as id";
                using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
                {
                    Kill emptyKill = await cnn.QueryFirstOrDefaultAsync<Kill>(findQuery);
                    if (emptyKill != null)
                    {
                        updateKillForGame(game_id, emptyKill.Kill_Id, newKill);

                        return emptyKill.Kill_Id;
                    }
                    else
                    {
                        int id = await cnn.ExecuteScalarAsync<int>(insertQuery, newKill);

                        return id;
                    }
                }
            } else
            {
                return -1;
            }
        }

        public static bool checkValidCode(int game_id, Kill newKill)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string killerQuery = $"SELECT * FROM Player WHERE game_id = '{game_id}' AND player_id='{newKill.Killer_Id}'";
                string sqlQuery = $"SELECT * FROM Player WHERE game_id = '{game_id}' AND bite_code='{newKill.Bite_Code}'";

                Player killer = cnn.Query<Player>(killerQuery).FirstOrDefault();
                Player victim = cnn.Query<Player>(sqlQuery).FirstOrDefault();

                if(killer == null || killer.Is_Human)
                {
                    return false;
                }

                if(victim != null)
                {
                    newKill.Victim_Id = victim.Player_Id;

                    PlayerProcessor.killPlayer(game_id, victim.Player_Id);

                    return true;
                } else
                {
                    return false;
                }
            }
        }

        public async static void updateKillForGame(int game_id, int kill_id, Kill kill)
        {
            string sqlQuery = "UPDATE \"Kill\" " +
                $"SET time_of_death='{kill.Time_Of_Death}', story='{kill.Story}', lat='{kill.Lat}', lng='{kill.Lng}', game_id='{kill.Game_Id}', killer_id='{kill.Killer_Id}', victim_id='{kill.Victim_Id}', bite_code='{kill.Bite_Code}' " +
                $"WHERE kill_id='{kill_id}'";

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public async static void deleteKill(int game_id, int kill_id)
        {
            string sqlQuery = "UPDATE \"Kill\" " +
                $"SET time_of_death='', story='', bite_code='' " +
                $"WHERE game_id='{game_id}' AND kill_id='{kill_id}' ";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public static string LoadConnectionString()
        {
            //return @"Data Source =.\HvZSqliteDB.db; Version = 3;";
            return @"Server = tcp:hvz.database.windows.net,1433; Initial Catalog = hvz; Persist Security Info = False; User ID = hvz; Password = Caseoppgave2019; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
        }
    }
}
