﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using HvZAPI.DBProcessors;
using HvZAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HvZAPI.Controllers
{
    [Authorize]
    [Route("game")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MissionController : ControllerBase
    {
        [HttpGet("{game_id}/mission")]
        public async Task<ActionResult<IEnumerable<Mission>>> GetMissionsByGameId(int game_id)
        {
            return await MissionProcessor.getMissionsByGameId(game_id);
        }

        [HttpGet("{game_id}/mission/{mission_id}")]
        public async Task<ActionResult<Mission>> GetMissionByMissionId(int game_id, int mission_id)
        {
            return await MissionProcessor.getMissionByMissionId(game_id, mission_id);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("{game_id}/mission")]
        public async Task<int> CreateMissionForGame(int game_id, [FromBody] Mission newMission)
        {
            try
            {
                return await MissionProcessor.createMissionForGame(game_id, newMission);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{game_id}/mission/{mission_id}")]
        public bool updateMissionForGame(int game_id, int mission_id, [FromBody] Mission mission)
        {
            try
            {
                MissionProcessor.updateMissionForGame(game_id, mission_id, mission);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{game_id}/mission/{mission_id}")]
        public bool deleteMission(int game_id, int mission_id)
        {
            try
            {
                MissionProcessor.deleteMission(game_id, mission_id);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}