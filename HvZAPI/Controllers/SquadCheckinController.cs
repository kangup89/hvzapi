﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using HvZAPI.DBProcessors;
using HvZAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HvZAPI.Controllers
{
    [Authorize]
    [Route("game")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SquadCheckinController : ControllerBase
    {
        [HttpGet("{game_id}/squad/{squad_id}/check-in")]
        public async Task<ActionResult<IEnumerable<SquadCheckin>>> GetSquadCheckinsBySquadId(int game_id, int squad_id)
        {
            return await SquadCheckinProcessor.getSquadCheckins(game_id, squad_id);
        }

        [HttpPost("{game_id}/squad/{squad_id}/check-in/")]
        public async Task<int> CreateSquadCheckin(int game_id, int squad_id, [FromBody] SquadCheckin newSquadCheckin)
        {
            try
            {
                return await SquadCheckinProcessor.createSquadCheckin(game_id, squad_id, newSquadCheckin);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{game_id}/squad/{squad_id}/check-in/{squad_checkin_id}")]
        public bool UpdateSquadCheckin(int game_id, int squad_id, int squad_checkin_id, [FromBody] SquadCheckin squadCheckin)
        {
            try
            {
                SquadCheckinProcessor.updateSquadCheckin(game_id, squad_id, squad_checkin_id, squadCheckin);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{game_id}/squad/{squad_id}/check-in/{squad_checkin_id}")]
        public bool DeleteSquadCheckin(int game_id, int squad_id, int squad_checkin_id)
        {
            try
            {
                SquadCheckinProcessor.deleteSquadCheckin(game_id, squad_id, squad_checkin_id);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}