﻿using Dapper;
using HvZAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.DBProcessors
{
    public class PlayerProcessor
    {
        public async static Task<List<Player>> getPlayersByGameId(int game_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Player WHERE game_id = '{game_id}' AND bite_code!=''";
                var output = await cnn.QueryAsync<Player>(sqlQuery);

                return output.ToList();
            }
        }

        public async static Task<Player> getPlayerByPlayerId(int game_id, int player_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Player WHERE game_id = '{game_id}' AND player_id='{player_id}'";
                Player output = await cnn.QueryFirstOrDefaultAsync<Player>(sqlQuery);

                return output;
            }
        }

        public async static Task<Player> getPlayerByUserId(int game_id, int user_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Player WHERE game_id = '{game_id}' AND user_id='{user_id}' AND bite_code!=''";
                Player output = await cnn.QueryFirstOrDefaultAsync<Player>(sqlQuery);

                DateTime currentTime = DateTime.Now;

                return output;
            }
        }

        public async static Task<int> regiPlayerForGame(int game_id, Player newPlayer) 
        {
            string findQuery = "SELECT * FROM Player WHERE is_human='false' AND is_patient_zero='false' AND bite_code='' ";
            string insertQuery = "INSERT INTO Player (is_human, is_patient_zero, bite_code, user_id, game_id, username) " +
                "VALUES (@Is_Human, @Is_Patient_Zero, @Bite_Code, @User_Id, @Game_Id, @Username) " +
                "SELECT SCOPE_IDENTITY() as id";
            //string findUsername = $"SELECT * FROM \"User\" WHERE user_id='{newPlayer.User_Id}' ";

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                //User user = await cnn.QueryFirstOrDefaultAsync<User>(findUsername);

                //if(user != null)
                //{
                    //newPlayer.Username = user.Username;

                Player emptyPlayer = await cnn.QueryFirstOrDefaultAsync<Player>(findQuery);

                if (emptyPlayer != null)
                {
                    return await updatePlayerForGame(game_id, emptyPlayer.Player_Id, newPlayer);

                    //return emptyPlayer.Player_Id;
                }
                else
                {
                    int id = await cnn.ExecuteScalarAsync<int>(insertQuery, newPlayer);

                    return id;
                }
                //} else
                //{
                //    return -1;
                //}
            }
        }

        public async static Task<int> updatePlayerForGame(int game_id, int player_id, Player player)
        {
            string sqlQuery = "UPDATE Player " +
                $"SET is_human='{player.Is_Human}', is_patient_zero='{player.Is_Patient_Zero}', bite_code='{player.Bite_Code}', user_id='{player.User_Id}', game_id='{player.Game_Id}', username='{player.Username}' " +
                $"WHERE player_id='{player_id}' " +
                 "SELECT SCOPE_IDENTITY() as id";

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);

                return player_id;
            }
        }

        public async static Task<int> updatePlayerState(int game_id, int player_id, Player player)
        {
            string sqlQuery = "UPDATE Player " +
                $"SET is_human='{player.Is_Human}', is_patient_zero='{player.Is_Patient_Zero}', bite_code='{player.Bite_Code}', user_id='{player.User_Id}', game_id='{player.Game_Id}', username='{player.Username}' " +
                $"WHERE player_id='{player_id}' " +
                 "SELECT SCOPE_IDENTITY() as id";

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                SquadMemberProcessor.deleteSquadMemberForPlayer(game_id, player_id);

                await cnn.ExecuteAsync(sqlQuery);

                return player_id;
            }
        }


        public async static void deletePlayer(int game_id, int player_id)
        {
            string sqlQuery = "UPDATE Player " +
                $"SET is_human='false', is_patient_zero='false', bite_code='', username='' " +
                $"WHERE player_id='{player_id}' ";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public async static void killPlayer(int game_id, int player_id)
        {
            string sqlQuery = "UPDATE Player " +
                $"SET is_human='false', bite_code='bittenbyazombie' " +
                $"WHERE player_id='{player_id}' AND game_id='{game_id}' ";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                SquadMemberProcessor.deleteSquadMemberForPlayer(game_id, player_id);

                Debug.WriteLine(sqlQuery);
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public static string LoadConnectionString()
        {
            //return @"Data Source =.\HvZSqliteDB.db; Version = 3;";
            return @"Server = tcp:hvz.database.windows.net,1433; Initial Catalog = hvz; Persist Security Info = False; User ID = hvz; Password = Caseoppgave2019; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
        }
    }
}
