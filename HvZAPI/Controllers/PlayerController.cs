﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using HvZAPI.DBProcessors;
using HvZAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HvZAPI.Controllers
{
    [Authorize]
    [Route("game")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PlayerController : ControllerBase
    {
        [AllowAnonymous]
        [HttpGet("{game_id}/player")]
        public async Task<ActionResult<IEnumerable<Player>>> GetPlayersByGameId(int game_id)
        {
            return await PlayerProcessor.getPlayersByGameId(game_id);
        }

        [HttpGet("{game_id}/player/{player_id}")]
        public async Task<ActionResult<Player>> GetPlayerById(int game_id, int player_id)
        {
            return await PlayerProcessor.getPlayerByPlayerId(game_id, player_id);
        }

        [HttpGet("{game_id}/user/{user_id}/player/")]
        public async Task<ActionResult<Player>> GetPlayerByUserId(int game_id, int user_id)
        {
            Player player = await PlayerProcessor.getPlayerByUserId(game_id, user_id);

            return player;
        }

        [HttpPost("{game_id}/player")]
        public async Task<int> RegiPlayerForGame(int game_id, [FromBody] Player newPlayer)
        {
            try
            {
                return await PlayerProcessor.regiPlayerForGame(game_id, newPlayer);
            } catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{game_id}/player/{player_id}")]
        public async Task<bool> UpdatePlayerForGame(int game_id, int player_id, [FromBody] Player player)
        {
            try
            {
                await PlayerProcessor.updatePlayerForGame(game_id, player_id, player);

                return true;
            } catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{game_id}/player/{player_id}/state")]
        public async Task<bool> UpdatePlayerState(int game_id, int player_id, Player player)
        {
            try
            {
                await PlayerProcessor.updatePlayerState(game_id, player_id, player);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }


        //[Authorize(Roles = "Admin")]
        [HttpDelete("{game_id}/player/{player_id}")]
        public bool deletePlayer(int game_id, int player_id)
        {
            try
            {
                PlayerProcessor.deletePlayer(game_id, player_id);

                return true;
            } catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
