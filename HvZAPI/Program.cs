﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace HvZAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

    //    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
    //      WebHost.CreateDefaultBuilder(args)
        //    .UseSetting("https_port", "5001")
        //    .UseStartup<Startup>();

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                //.UseSetting("https_port", "5001")
                .UseStartup<Startup>()
                //.UseUrls("http://localhost:4000")
                .Build();
    }
}
