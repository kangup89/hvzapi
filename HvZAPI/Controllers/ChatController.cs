﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HvZAPI.DBProcessors;
using HvZAPI.Models;
using HvZAPI.Models.DBProcessors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;

namespace HvZAPI.Controllers
{
    [Authorize]
    [Route("game")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ChatController : ControllerBase
    {
        [HttpGet("{game_id}/chat/")]
        public async Task<ActionResult<IEnumerable<Chat>>> GetChatsForGame(int game_id)
        {
            return await ChatProcessor.getChatsForGame(game_id);
        }

        [HttpGet("{game_id}/chat/global")]
        public async Task<ActionResult<IEnumerable<Chat>>> GetGlobalChatsForGame(int game_id)
        {
            return await ChatProcessor.getGlobalChatsForGame(game_id);
        }

        // GET game/:id/chat/human
        [HttpGet("{game_id}/chat/human")]
        public async Task<ActionResult<IEnumerable<Chat>>> GetHumanChat(int game_id)
        {
            return await ChatProcessor.getHumanChat(game_id);
        }

        // GET game/:id/chat/zombie
        [HttpGet("{game_id}/chat/zombie")]
        public async Task<ActionResult<IEnumerable<Chat>>> GetZombieChat(int game_id)
        {
            return await ChatProcessor.getZombieChat(game_id);
        }

        // GET game/:game_id/chat/squad/:squad_id
        [HttpGet("{game_id}/chat/squad/{squad_id}")]
        public async Task<ActionResult<IEnumerable<Chat>>> GetSquadChat(int game_id, int squad_id)
        {
            return await ChatProcessor.getSquadChat(game_id, squad_id);
        }

        // POST game/:id/chat
        [HttpPost("{game_id}/chat")]
        public async Task<int> SendNewMessage(int game_id, [FromBody] Chat newChat)
        {
            try
            {
                return await ChatProcessor.sendNewMessage(game_id, newChat);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{game_id}/chat/{message_id}")]
        public bool UpdateMessage(int game_id, int message_id, [FromBody] Chat chat)
        {
            try
            {
                ChatProcessor.updateMessage(game_id, message_id, chat);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{game_id}/chat/{message_id}")]
        public bool DeleteMessage(int game_id, int message_id)
        {
            try
            {
                ChatProcessor.deleteMessage(game_id, message_id);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}