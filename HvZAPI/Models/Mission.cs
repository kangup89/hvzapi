﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.Models
{
    public class Mission
    {
        public int Mission_Id { get; set; }
        public string Name { get; set; }
        public bool Is_Human_Visible { get; set; }
        public bool Is_Zombie_Visible { get; set; }
        public string Description { get; set; }
        public DateTime Start_Time { get; set; }
        public DateTime End_Time { get; set; }
        public int Game_Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Mission()
        {
        }
    }
}
