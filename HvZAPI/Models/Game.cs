﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.Models
{
    public class Game
    {
        public int Game_Id { get; set; }
        public string Name { get; set; }
        public string Game_State { get; set; }
        public double Nw_Lat { get; set; }
        public double Nw_Lng { get; set; }
        public double Se_Lat { get; set; }
        public double Se_Lng { get; set; }
        public string Description { get; set; }
        public DateTime Start_Time { get; set; }
        public DateTime End_Time { get; set; }
        public bool Has_Patient_Zero { get; set; }

        public Game()
        {

        }
    }
}
