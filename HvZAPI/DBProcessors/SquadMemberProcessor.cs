﻿using Dapper;
using HvZAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.DBProcessors
{
    public class SquadMemberProcessor
    {
        public async static Task<List<SquadMember>> getSquadMembersBySquadId(int game_id, int squad_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Squad_Member WHERE game_id = '{game_id}' AND squad_id = '{squad_id}' AND rank! = '255'";
                var output = await cnn.QueryAsync<SquadMember>(sqlQuery);

                return output.ToList();
            }
        }

        public async static Task<SquadMember> getSquadMemberByMemberId(int game_id, int squad_id, int squad_member_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Squad_Member WHERE game_id = '{game_id}' AND squad_id = '{squad_id}' AND squad_member_id='{squad_member_id}'";
                SquadMember output = await cnn.QueryFirstOrDefaultAsync<SquadMember>(sqlQuery);

                return output;
            }
        }

        public async static Task<SquadMember> getSquadMemberByPlayerId(int game_id, int player_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Squad_Member WHERE game_id = '{game_id}' AND player_id = '{player_id}' AND rank! = '255' ";
                SquadMember output = await cnn.QueryFirstOrDefaultAsync<SquadMember>(sqlQuery);

                return output;
            }
        }

        public async static Task<int> createSquadMember(int game_id, int squad_id, SquadMember newSquadMember)
        {
            newSquadMember = await fillProperties(game_id, newSquadMember);

            string findQuery = "SELECT * FROM Squad_Member WHERE username='' AND rank='255' ";
            string insertQuery = "INSERT INTO Squad_Member (username, rank, is_human,  game_id, squad_id, player_id) " +
                "VALUES (@Username, @Rank, @Is_Human, @Game_Id, @Squad_Id, @Player_Id)" +
                "SELECT SCOPE_IDENTITY() as id";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                SquadMember emptySquadMember = await cnn.QueryFirstOrDefaultAsync<SquadMember>(findQuery);
                if (emptySquadMember != null)
                {
                    updateSquadMember(game_id, squad_id, emptySquadMember.Squad_Member_Id, newSquadMember);

                    return emptySquadMember.Squad_Member_Id;
                }
                else
                {
                    int id = await cnn.ExecuteScalarAsync<int>(insertQuery, newSquadMember);

                    return id;
                }
            }
        }

        public async static Task<SquadMember> fillProperties(int game_id, SquadMember newSquadMember)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Player WHERE game_id = '{newSquadMember.Game_Id}' AND player_id = '{newSquadMember.Player_Id}'";
                Player player = await cnn.QueryFirstOrDefaultAsync<Player>(sqlQuery);

                newSquadMember.Is_Human = player.Is_Human;

                sqlQuery = $"SELECT * FROM Squad_Member WHERE game_id = '{newSquadMember.Game_Id}' AND squad_id = '{newSquadMember.Squad_Id}' AND rank != '255' ";
                var members = await cnn.QueryAsync<SquadMember>(sqlQuery);

                newSquadMember.Rank = members.ToList().Count() + 1;

                sqlQuery = $"SELECT * FROM \"User\" WHERE user_id = ' {player.User_Id}'";
                User user = await cnn.QueryFirstOrDefaultAsync<User>(sqlQuery);

                newSquadMember.Username = user.Username;
                //Debug.WriteLine("user.Username");

                return newSquadMember;
            }
        }

        public async static void updateSquadMember(int game_id, int squad_id, int squad_member_id, SquadMember squadMember)
        {
            string sqlQuery = "UPDATE Squad_Member " +
                $"SET username='{squadMember.Username}', rank='{squadMember.Rank}', is_human='{squadMember.Is_Human}', game_id='{squadMember.Game_Id}', squad_id='{squadMember.Squad_Id}', player_id='{squadMember.Player_Id}' " +
                $"WHERE squad_member_id='{squad_member_id}'";

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public async static void deleteSquadMember(int game_id, int squad_id, int squad_member_id)
        {
            string sqlQuery = "UPDATE Squad_Member " +
                $"SET username='', rank='255' " +
                $"WHERE squad_member_id='{squad_member_id}' ";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                List<SquadMember> members = await getSquadMembersBySquadId(game_id, squad_id);
                if(members.Count == 1)
                {
                    SquadProcessor.deleteSquad(game_id, squad_id);
                }

                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public async static void deleteSquadMemberForPlayer(int game_id, int player_id) {
            string sqlQuery = "UPDATE Squad_Member " +
                $"SET username='', rank='255' " +
                $"WHERE game_id='{game_id}' AND player_id='{player_id}' ";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                SquadMember member = await getSquadMemberByPlayerId(game_id, player_id);

                List<SquadMember> squadMembers = await getSquadMembersBySquadId(game_id, member.Squad_Id);
                if (squadMembers.Count == 1)
                {
                    SquadProcessor.deleteSquad(game_id, member.Squad_Id);
                }

                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public static string LoadConnectionString()
        {
            //return @"Data Source =.\HvZSqliteDB.db; Version = 3;";
            return @"Server = tcp:hvz.database.windows.net,1433; Initial Catalog = hvz; Persist Security Info = False; User ID = hvz; Password = Caseoppgave2019; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
        }
    }
}
