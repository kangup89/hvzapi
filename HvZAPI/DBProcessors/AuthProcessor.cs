﻿using Dapper;
using HvZAPI.Helpsers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HvZAPI.Models.DBProcessors
{
    public class AuthProcessor
    {
        private readonly AppSettings _appSettings;

        public AuthProcessor(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public async static Task<List<User>> getAllUsers()
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                //string sqlQuery = $"SELECT first_name, last_name, username, is_admin FROM \"User\" WHERE username!='' AND password!=''";
                string sqlQuery = $"SELECT * FROM \"User\" WHERE username!='' OR password!='' OR password_hash!='' ";
                var output = await cnn.QueryAsync<User>(sqlQuery, new DynamicParameters());

                return output.ToList();
            }
        }

        public async Task<User> authenticate(User userInfo)
        {
            string username = userInfo.Username;
            string password = userInfo.Password;

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT user_id, first_name, last_name, username, is_admin, token FROM \"User\" WHERE username='{username}' AND password='{password}'";
                //string sqlQuery = $"SELECT user_id, first_name, last_name, username, is_admin, token FROM \"User\" WHERE username='{username}' AND  password_hash=HASHBYTES('SHA2_512', '{password}') ";

                User user = await cnn.QueryFirstOrDefaultAsync<User>(sqlQuery);

                if(user == null)
                {
                    return null;
                }

                string userRole;

                if(user.Is_Admin)
                {
                    userRole = "Admin";
                } else
                {
                    userRole = "Player";
                }

                var tokenHandler = new JwtSecurityTokenHandler();
                //Debug.WriteLine("Secret!!: " + _appSettings.Secret);
                //var key = Convert.FromBase64String("HvZCaseOppgave");
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.NameIdentifier, user.User_Id.ToString()),
                        new Claim(ClaimTypes.Name, user.Username),
                        new Claim(ClaimTypes.Name, user.First_Name),
                        new Claim(ClaimTypes.Name, user.Last_Name),
                        new Claim(ClaimTypes.Role, userRole)
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                user.Token = tokenHandler.WriteToken(token);

                user.Password = "";

                return user;  
            }
        }

        public async static Task<int> createNewUser(User newUser)
        {
            string findQuery = $"SELECT * FROM \"User\" WHERE username='{newUser.Username}' ";
            string findEmptyQuery = $"SELECT * FROM \"User\" WHERE username='' AND password='' ";
            string insertQuery = "insert into \"user\" (first_name, last_name, username, password, is_admin) " +
                "values (@first_name, @last_name, @username, @password, @is_admin) " +
                "select scope_identity() as id";
            //string insertQuery = "INSERT INTO \"User\" (first_name, last_name, username, password_hash, is_admin) " +
            //    "VALUES (@First_Name, @Last_Name, @Username, HASHBYTES('SHA2_512', @Password), @Is_Admin) " +
            //    "SELECT SCOPE_IDENTITY() as id";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                User user = await cnn.QueryFirstOrDefaultAsync<User>(findQuery);

                if (user == null)
                {
                    User emptyUser = await cnn.QueryFirstOrDefaultAsync<User>(findEmptyQuery);
                    if (emptyUser != null)
                    {
                        updateUser(emptyUser.User_Id, newUser);

                        return emptyUser.User_Id;
                    }
                    else
                    {
                        int id = await cnn.ExecuteScalarAsync<int>(insertQuery, newUser);

                        //string idQuery = "SELECT TOP 1 * FROM Mission ORDER BY mission_id DESC";
                        //Mission output = cnn.Query<Mission>(idQuery, new DynamicParameters()).FirstOrDefault();

                        return id;
                    }
                }
                else
                {
                    return -1;
                }
            }
        }

        public async static void updateUser(int user_id, User user)
        {
            var bytes = System.Text.Encoding.UTF8.GetBytes(user.Password);
            using (var hash = System.Security.Cryptography.SHA512.Create())
            {
                byte[] hashedInputBytes = hash.ComputeHash(bytes);

                string sqlQuery = "UPDATE \"User\" " +
                $"SET first_name='{user.First_Name}', last_name='{user.Last_Name}', username='{user.Username}', is_admin='{user.Is_Admin}', password='{user.Password}', password_hash=HASHBYTES('SHA2_512', '{user.Password}') " +
                $"WHERE user_id='{user_id}' ";

                using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
                {
                    await cnn.ExecuteAsync(sqlQuery);
                }
            }
        }

        public async static void deleteUser(int user_id)
        {
            string sqlQuery = "UPDATE \"User\" " +
                $"SET first_name='', last_name='', username='', password='', is_admin='false' " +
                $"WHERE user_id='{user_id}'";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public static string LoadConnectionString()
        {
            //return @"Data Source =.\HvZSqliteDB.db; Version = 3;";
            return @"Server = tcp:hvz.database.windows.net,1433; Initial Catalog = hvz; Persist Security Info = False; User ID = hvz; Password = Caseoppgave2019; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
        }
    }
}
