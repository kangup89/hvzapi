﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.Models
{
    public class Chat
    {
        public int Message_Id { get; set; }
        public string Message { get; set; }
        public bool Is_Human_Global { get; set; }
        public bool Is_Zombie_Global { get; set; }
        public DateTime Chat_Time { get; set; }
        public int Game_Id { get; set; }
        public int Player_Id { get; set; }
        public int Squad_Id { get; set; }
        public string Username { get; set; }
        public bool is_admin { get; set; }

        public Chat()
        {
        }
    }
}
