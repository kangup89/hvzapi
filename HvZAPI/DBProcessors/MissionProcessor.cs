﻿using Dapper;
using HvZAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.DBProcessors
{
    public class MissionProcessor
    {
        public async static Task<List<Mission>> getMissionsByGameId(int game_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Mission WHERE game_id = '{game_id}' AND name!='' ";
                var output = await cnn.QueryAsync<Mission>(sqlQuery);

                return output.ToList();
            }
        }

        public async static Task<Mission> getMissionByMissionId(int game_id, int mission_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Mission WHERE game_id = '{game_id}' AND mission_id='{mission_id}'";
                Mission output = await cnn.QueryFirstOrDefaultAsync<Mission>(sqlQuery);

                return output;
            }
        }

        public async static Task<int> createMissionForGame(int game_id, Mission newMission)
        {
            string findQuery = "SELECT * FROM Mission WHERE name='' AND is_human_visible='false' AND is_zombie_visible='false' ";
            string insertQuery = "INSERT INTO Mission (name, is_human_visible, is_zombie_visible, description, start_time, end_time, game_id, latitude, longitude) " +
                "VALUES (@Name, @Is_Human_Visible, @Is_Zombie_Visible, @Description, @Start_Time, @End_Time, @Game_Id, @Latitude, @Longitude) " +
                "SELECT SCOPE_IDENTITY() as id";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                Mission emptyMission = await cnn.QueryFirstOrDefaultAsync<Mission>(findQuery);
                if(emptyMission != null)
                {
                    updateMissionForGame(game_id, emptyMission.Mission_Id, newMission);

                    return emptyMission.Mission_Id;
                } else
                {
                    int id = await cnn.ExecuteScalarAsync<int>(insertQuery, newMission);

                    return id;
                }
            }
        }

        public async static void updateMissionForGame(int game_id, int mission_id, Mission mission)
        {
            string sqlQuery = "UPDATE Mission " +
                $"SET name='{mission.Name}', is_human_visible='{mission.Is_Human_Visible}', is_zombie_visible='{mission.Is_Zombie_Visible}', description='{mission.Description}', start_time='{mission.Start_Time}', end_time='{mission.End_Time}', game_id='{mission.Game_Id}', latitude='{mission.Latitude}', longitude='{mission.Longitude}' " +
                $"WHERE mission_id='{mission_id}'";

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public async static void deleteMission(int game_id, int mission_id)
        {
            string sqlQuery = "UPDATE Mission " +
                $"SET name='', is_human_visible='false', is_zombie_visible='false' " +
                $"WHERE mission_id='{mission_id}' ";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public static string LoadConnectionString()
        {
            //return @"Data Source =.\HvZSqliteDB.db; Version = 3;";
            return @"Server = tcp:hvz.database.windows.net,1433; Initial Catalog = hvz; Persist Security Info = False; User ID = hvz; Password = Caseoppgave2019; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
        }
    }
}
