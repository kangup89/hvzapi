﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.Models
{
    public class Player
    {
        public int Player_Id { get; set; }
        public bool Is_Human { get; set; }
        public bool Is_Patient_Zero { get; set; }
        public string Bite_Code { get; set; }
        public int User_Id { get; set; }
        public int Game_Id { get; set; }
        public string Username { get; set; }

        public Player()
        {
        }
    }
}
