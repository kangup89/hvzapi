﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using HvZAPI.DBProcessors;
using HvZAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HvZAPI.Controllers
{
    [Authorize]
    [Route("game")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SquadMemberController : ControllerBase
    {
        [HttpGet("{game_id}/squad/{squad_id}/member")]
        public async Task<ActionResult<IEnumerable<SquadMember>>> GetSquadMembersBySquadId(int game_id, int squad_id)
        {
            return await SquadMemberProcessor.getSquadMembersBySquadId(game_id, squad_id);
        }

        [HttpGet("{game_id}/squad/{squad_id}/member/{squad_member_id}")]
        public async Task<ActionResult<SquadMember>> GetSquadMemberByMemberId(int game_id, int squad_id, int squad_member_id)
        {
            return await SquadMemberProcessor.getSquadMemberByMemberId(game_id, squad_id, squad_member_id);
        }

        [HttpGet("{game_id}/member/{player_id}")]
        public async Task<ActionResult<SquadMember>> GetSquadMemberByPlayerId(int game_id, int player_id)
        {
            return await SquadMemberProcessor.getSquadMemberByPlayerId(game_id, player_id);
        }
        
        [HttpPost("{game_id}/squad/{squad_id}/member")]
        public async Task<int> CreateSquadMember(int game_id, int squad_id, [FromBody] SquadMember newSquadMember)
        {
            try
            {
                return await SquadMemberProcessor.createSquadMember(game_id, squad_id, newSquadMember);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{game_id}/squad/{squad_id}/member/{squad_member_id}")]
        public bool UpdateSquadMember(int game_id, int squad_id, int squad_member_id, [FromBody] SquadMember squadMember)
        {
            try
            {
                SquadMemberProcessor.updateSquadMember(game_id, squad_id, squad_member_id, squadMember);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        [HttpDelete("{game_id}/squad/{squad_id}/member/{squad_member_id}")]
        public bool DeleteSquadMember(int game_id, int squad_id, int squad_member_id)
        {
            try
            {
                SquadMemberProcessor.deleteSquadMember(game_id, squad_id, squad_member_id);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}