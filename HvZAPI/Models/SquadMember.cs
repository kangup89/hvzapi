﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.Models
{
    public class SquadMember
    {
        public int Squad_Member_Id { get; set; }
        public string Username { get; set; }
        public int Rank { get; set; }
        public bool Is_Human { get; set; }
        public int Game_Id { get; set; }
        public int Squad_Id { get; set; }
        public int Player_Id { get; set; }

        public SquadMember()
        {
        }
    }
}
