﻿using Dapper;
using HvZAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.DBProcessors
{
    public class SquadCheckinProcessor
    {
        public async static Task<List<SquadCheckin>> getSquadCheckins(int game_id, int squad_id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"SELECT * FROM Squad_Checkin WHERE game_id = '{game_id}' AND squad_id = '{squad_id}' ";
                var output = await cnn.QueryAsync<SquadCheckin>(sqlQuery, new DynamicParameters());

                return output.ToList();
            }
        }

        public async static Task<int> createSquadCheckin(int game_id, int squad_id, SquadCheckin newCheckin)
        {
            string findQuery = "SELECT * FROM Squad_Checkin WHERE start_time='' AND end_time='' ";
            string insertQuery = "INSERT INTO Squad_Checkin (start_time, end_time, lat, lng, game_id, squad_id, squad_member_id) " +
                "VALUES (@Start_Time, @End_Time, @Lat, @Lng, @Game_Id, @Squad_Id, @Squad_Member_Id) " +
                "SELECT SCOPE_IDENTITY() as id";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                SquadCheckin emptySquadCheckin = await cnn.QueryFirstOrDefaultAsync<SquadCheckin>(findQuery);
                if (emptySquadCheckin != null)
                {
                    updateSquadCheckin(game_id, squad_id, emptySquadCheckin.Squad_Checkin_Id, newCheckin);

                    return emptySquadCheckin.Squad_Checkin_Id;
                }
                else
                {
                    int id = await cnn.ExecuteScalarAsync<int>(insertQuery, newCheckin);

                    return id;
                }
            }
        }

        public async static void updateSquadCheckin(int game_id, int squad_id, int squad_checkin_id, SquadCheckin squadCheckin)
        {
            string sqlQuery = "UPDATE Squad_Checkin " +
                $"SET start_time='{squadCheckin.Start_Time}', end_time='{squadCheckin.End_Time}', lat='{squadCheckin.Lat}', lng='{squadCheckin.Lng}', game_id='{squadCheckin.Game_Id}', squad_id='{squadCheckin.Squad_Id}', squad_member_id='{squadCheckin.Squad_Member_Id}' " +
                $"WHERE squad_checkin_id='{squad_checkin_id}'";

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public async static void deleteSquadCheckin(int game_id, int squad_id, int squad_checkin_id)
        {
            string sqlQuery = "UPDATE Squad_Checkin " +
                $"SET lat='0', lng='0', start_time='', end_time='' " +
                $"WHERE squad_checkin_id='{squad_checkin_id}'";

            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                await cnn.ExecuteAsync(sqlQuery);
            }
        }

        public static string LoadConnectionString()
        {
            //return @"Data Source =.\HvZSqliteDB.db; Version = 3;";
            return @"Server = tcp:hvz.database.windows.net,1433; Initial Catalog = hvz; Persist Security Info = False; User ID = hvz; Password = Caseoppgave2019; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
        }
    }
}
