﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZAPI.Models
{
    public class SquadCheckin
    {
        public int Squad_Checkin_Id { get; set; }
        public DateTime Start_Time { get; set; }
        public DateTime End_Time { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Game_Id { get; set; }
        public int Squad_Id { get; set; }
        public int Squad_Member_Id { get; set; }

        public SquadCheckin()
        {
        }
    }
}
